#include "script.h"
#include "keyboard.h"


#include <string>
#include <ctime>
#include "Pattern.h"
#include <Psapi.h>
using namespace std;

#pragma warning(disable : 4244 4305) // double <-> float conversions

using namespace std;
#include "MenuFunctions.h"

char *model;
HANDLE mainFiber;
DWORD wakeAt;
bool neverwanted = false;

void ClearWantedLevel(Player player)
{
	PLAYER::CLEAR_PLAYER_WANTED_LEVEL(player);
}

void RequestControl(Entity e)
{
	NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(e);
	if (!NETWORK::NETWORK_HAS_CONTROL_OF_ENTITY(e))
		WAIT(0);
	NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(e);
}

void BypassOnlineVehicleKick(Vehicle vehicle)
{
	Player player = PLAYER::PLAYER_ID();
	int var;
	DECORATOR::DECOR_REGISTER("Player_Vehicle", 3);
	DECORATOR::DECOR_REGISTER("Veh_Modded_By_Player", 3);
	DECORATOR::DECOR_SET_INT(vehicle, "Player_Vehicle", NETWORK::_0xBC1D768F2F5D6C05(player));
	DECORATOR::DECOR_SET_INT(vehicle, "Veh_Modded_By_Player", GAMEPLAY::GET_HASH_KEY(PLAYER::GET_PLAYER_NAME(player)));
	DECORATOR::DECOR_SET_INT(vehicle, "Not_Allow_As_Saved_Veh", 0);
	if (DECORATOR::DECOR_EXIST_ON(vehicle, "MPBitset"))
	{
		var = DECORATOR::DECOR_GET_INT(vehicle, "MPBitset");
	}
	GAMEPLAY::SET_BIT(&var, 3);
	DECORATOR::DECOR_SET_INT(vehicle, "MPBitset", var);
	VEHICLE::SET_VEHICLE_IS_STOLEN(vehicle, false);
}

Vehicle veh;

int CREATE_VEHICLE(char* hash, float x, float y, float z)
{
	DWORD model = GAMEPLAY::GET_HASH_KEY(hash);
	if (STREAMING::IS_MODEL_IN_CDIMAGE(model) && STREAMING::IS_MODEL_A_VEHICLE(model))
	{
		Player playerPed = PLAYER::PLAYER_PED_ID();
		STREAMING::REQUEST_MODEL(model);
		STREAMING::REQUEST_NAMED_PTFX_ASSET("scr_rcbarry2");
		while (!STREAMING::HAS_MODEL_LOADED(model) && !STREAMING::HAS_PTFX_ASSET_LOADED()) WAIT(0);
		{
			Vehicle veh = VEHICLE::CREATE_VEHICLE(model, x, y, z, ENTITY::GET_ENTITY_HEADING(playerPed), TRUE, TRUE);
			NETWORK::NETWORK_REQUEST_CONTROL_OF_ENTITY(veh);
			BypassOnlineVehicleKick(veh);
			VEHICLE::SET_VEHICLE_ENGINE_ON(veh, TRUE, TRUE);
		}
	}
	return veh;
}


void addVehOption(char* option, char* model11, char *notification)
{
	addOption(option);
	if (currentOption == optionCount && optionPress)
	{
		Vector3 coords = ENTITY::GET_ENTITY_COORDS(PLAYER::PLAYER_PED_ID(), true);
		CREATE_VEHICLE(model11, coords.x, coords.y, coords.z);
		drawNotification(notification);
	}
}

char* VehicleString;
bool SpawningVehicle;

void SpawnCar(char *carName)
{
	VehicleString = carName;
	SpawningVehicle = true;
}



void Main()
{

	addTitle("KevvvZuidWest 1.0");
	addSubmenuOption("Self Options			>>", 2);
	addSubmenuOption("Players List			>>", 3);
	addSubmenuOption("Vehicle Spawner		>>", 5);
	addSubmenuOption("Vehicle Menu			>>", 7);
	addSubmenuOption("Credits				>>", 9);

	normalMenuActions();
}

void VehMenu()
{
	Vehicle Veh;
	Player selfPlayer = PLAYER::PLAYER_ID();
	Ped selfPed = PLAYER::PLAYER_PED_ID();
	Vehicle selfVehicle = PED::GET_VEHICLE_PED_IS_IN(selfPed, FALSE);
	Vector3 playerPos = ENTITY::GET_ENTITY_COORDS(selfPed, TRUE);

	addTitle("Vehicle Menu");
	addOption("Fix and Clean");
	addOption("Upgrade Vehicle");
	addOption("Rainbow Car");

	if (getOption() == 1)
	{
		
		VEHICLE::SET_VEHICLE_FIXED(selfVehicle);
		VEHICLE::SET_VEHICLE_DEFORMATION_FIXED(selfVehicle);
	    VEHICLE::SET_VEHICLE_DIRT_LEVEL(selfVehicle, 0.1);
		
		drawNotification("Vehicle Fixed");
	}

	if (getOption() == 2)
	{
		VEHICLE::SET_VEHICLE_MOD_KIT(selfVehicle, 0);
		VEHICLE::SET_VEHICLE_COLOURS(selfVehicle, 120, 120);
		VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(selfVehicle, "KEVVV");
		VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(selfVehicle, 1);
		VEHICLE::TOGGLE_VEHICLE_MOD(selfVehicle, 18, 1);
		VEHICLE::TOGGLE_VEHICLE_MOD(selfVehicle, 22, 1);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 16, 5, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 12, 2, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 11, 3, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 14, 14, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 15, 3, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 13, 2, 0);
		VEHICLE::SET_VEHICLE_WHEEL_TYPE(selfVehicle, 6);
		VEHICLE::SET_VEHICLE_WINDOW_TINT(selfVehicle, 5);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 23, 19, 1);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 0, 1, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 1, 1, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 2, 1, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 3, 1, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 4, 1, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 5, 1, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 6, 1, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 7, 1, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 8, 1, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 9, 1, 0);
		VEHICLE::SET_VEHICLE_MOD(selfVehicle, 10, 1, 0);
		
		drawNotification("Vehicle Fully Upgraded");
	}

	if (getOption() == 3)
	{
		drawNotification("Not Working YET");
		
	}

	normalMenuActions();
}

void Credits()
{
	addTitle("Credits");
	addOption("BiG_GHOST_GaMeR = Base");
	addOption("Kevvvzuidwest = Creator");
	normalMenuActions();
}

void SelPVeh()
{
	Ped selectedPed = PLAYER::GET_PLAYER_PED(selectedPlayer);
	addTitle("Player Vehicle");
	addOption("Kill Tyres");
	
	if (getOption() == 1)
	{
		if (PED::IS_PED_IN_ANY_VEHICLE(selectedPed, FALSE))
		{
			//fuck up the tires
			Vehicle selectedVehicle = PED::GET_VEHICLE_PED_IS_USING(selectedPed);
			RequestControl(selectedVehicle);
			VEHICLE::SET_VEHICLE_TYRES_CAN_BURST(selectedVehicle, TRUE);
			static int tireID = 0;
			for (tireID = 0; tireID < 8; tireID++)
			{
				VEHICLE::SET_VEHICLE_TYRE_BURST(selectedVehicle, tireID, true, 1000.0);
			}
		}
	}

	normalMenuActions();
}

void Selfmenu()
{

	Player selfPlayer = PLAYER::PLAYER_ID();
	Ped selfPed = PLAYER::PLAYER_PED_ID();
	addTitle("Self Options");
	addOption("Clear Wanted");
	addOption("God Mode : On");
	addOption("God Mode : Off");
	addOption("Teleport Test");

	if (getOption() == 1)
	{
		//ClearWanted moet hier worden aangeroepen ofzo
		PLAYER::CLEAR_PLAYER_WANTED_LEVEL
		(selfPlayer);
	}

	if (getOption() == 2)
	{
		PLAYER::SET_PLAYER_INVINCIBLE(selfPlayer, true);
		PED::CLEAR_PED_BLOOD_DAMAGE(selfPed);
		PED::RESET_PED_VISIBLE_DAMAGE(selfPed);
		drawNotification("God Mode Enabled");
	}

	if (getOption() == 3)
	{
		PLAYER::SET_PLAYER_INVINCIBLE(selfPlayer, false);

		drawNotification("God Mode Disabled");
	}

	if (getOption() == 4)
	{
		//Vector3 Coords == 
		//uint handle = selfPed;
		//if (PED::IS_PED_IN_ANY_VEHICLE(handle, 0))
			//handle = PED::GET_VEHICLE_PED_IS_IN(handle, 0);
		//ENTITY::SET_ENTITY_COORDS(handle, & Coords, 0, 0, 0, 1);
	}

	normalMenuActions();
}


void PlayersList()
{
	addTitle("Players List");

	for (int i = 0; i < 30; i++)
	{
		addOption(PLAYER::GET_PLAYER_NAME(i));
	}
	if (optionPress && PLAYER::IS_PLAYER_PLAYING(currentOption - 1))
	{
		selectedPlayer = currentOption - 1;
		changeSubmenu(4);
	}
	normalMenuActions();
}



int SelectedClient;
void TeleportToClient(int Client)
{
	Vector3 Coords = ENTITY::GET_ENTITY_COORDS(Client, 1);
	if (PED::IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER::PLAYER_PED_ID()))
		ENTITY::SET_ENTITY_COORDS(PED::GET_VEHICLE_PED_IS_USING(PLAYER::PLAYER_PED_ID()), Coords.x, Coords.y, Coords.z, 1, 0, 0, 1);
	else
		ENTITY::SET_ENTITY_COORDS(PLAYER::PLAYER_PED_ID(), Coords.x, Coords.y, Coords.z, 1, 0, 0, 1);
}

void OnlinePlayerMods()
{
	Ped selectedPed = PLAYER::GET_PLAYER_PED(selectedPlayer);
	addTitle(PLAYER::GET_PLAYER_NAME(selectedPlayer));
	addOption("Vehicle Menu			>>");
	addOption("Remove Weapons");
	addOption("Clone Ped");
	addOption("Kill Player");
	addOption("Teleport To Player");


	if (getOption() == 1)
	{
		changeSubmenu(8);
	}

	if (getOption() == 2)
	{
		WEAPON::REMOVE_ALL_PED_WEAPONS(selectedPed, true);
	}

	if (getOption() == 3)
	{
		PED::CLONE_PED(selectedPed, ENTITY::GET_ENTITY_HEADING(selectedPed), 1, 1);
	}
	
	if (getOption() == 4)
	{
		Vector3 playerPosition = ENTITY::GET_ENTITY_COORDS(selectedPed, FALSE);
		AI::CLEAR_PED_TASKS_IMMEDIATELY(selectedPed);
		FIRE::ADD_OWNED_EXPLOSION(selectedPed, playerPosition.x, playerPosition.y, playerPosition.z, EXPLOSION_TANKER, 1000.0f, FALSE, TRUE, 0.0f);
	}

	if (getOption() == 5)
	{
		TeleportToClient(PLAYER::GET_PLAYER_PED(selectedPlayer));
		STREAMING::REQUEST_NAMED_PTFX_ASSET("scr_rcbarry2");
		GRAPHICS::_SET_PTFX_ASSET_NEXT_CALL("scr_rcbarry2");
		GRAPHICS::START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_clown_appears", PLAYER::PLAYER_PED_ID(), 0.0, 0.0, -0.5, 0.0, 0.0, 0.0, 1.0, false, false, false);
	}

	normalMenuActions();
}

void VehSpawner()
{
	addTitle("Vehicle Spawner");
	addOption("PC Vehicles");

	if (getOption() == 1)
	{
		changeSubmenu(6);
	}
	
	normalMenuActions();
}



void PCCars()
{
	DRAW_TEXTURE("shopui_title_supermod", "shopui_title_supermod", titlebox, 0.1175f, 0.23f, 0.083f, 0, 255, 255, 255, 255);
	addVehOption("Duke of Death", "dukes2", "Spawned a ~b~Duke of Death");

	normalMenuActions();
}

Vector3 obje;
Vector3 fff;

DWORD64 GetModuleBase(HANDLE hProc, string &sModuleName)
{
	HMODULE *hModules;
	hModules = 0;
	char szBuf[50];
	DWORD cModules = 0;
	DWORD64 dwBase = 0;

	EnumProcessModules(hProc, hModules, 0, &cModules);
	hModules = new HMODULE[cModules / sizeof(HMODULE)];

	if (EnumProcessModules(hProc, hModules, cModules / sizeof(HMODULE), &cModules)) {
		for (int i = 0; i < cModules / sizeof(HMODULE); ++i) {
			if (GetModuleBaseName(hProc, hModules[i], szBuf, sizeof(szBuf))) {
				if (sModuleName.compare(szBuf) == 0) {
					dwBase = (DWORD64)hModules[i];
					break;
				}
			}
		}
	}
	return dwBase;
}

void bypass_online()
{
	__int64 Address = GetModuleBase(GetCurrentProcess(), string("GTA5.exe"));

	CHAR *MemoryBuff = new CHAR[4096];
	HANDLE hProcess = GetCurrentProcess();
	BYTE bytes[10] = { 0x48, 0x8B, 0x88, 0x10, 0x01, 0x00, 0x00, 0x48, 0x8B, 0xC1 };//BYPASS_SPAWN
	BYTE bytes2[10] = { 0x83, 0xBB, 0x34, 0x01, 0x00, 0x00, 0xFF, 0x0F, 0x95, 0xC0 };//BYPASS_EVERY_TIME_CHECK_FOR_PLAYER_MODEL
	BYTE nop2[2] = { 0x90, 0x90 };
	BYTE nop3[3] = { 0x90, 0x90, 0x90 };

	int Check = 0;
	for (;;)
	{
		ReadProcessMemory(hProcess, (LPVOID)Address, (LPVOID)MemoryBuff, 4096, NULL);
		for (INT p = 0; p < 4096; p++)
		{
			Address++;
			MemoryBuff++;
			if (memcmp(MemoryBuff, bytes, 10) == 0)
			{
				WriteProcessMemory(hProcess, (LPVOID)(Address + 0x20), nop2, 2, NULL);
				WriteProcessMemory(hProcess, (LPVOID)(Address + 0x2D), nop2, 2, NULL);
				Check += 1;
			}
			if (memcmp(MemoryBuff, bytes2, 10) == 0)
			{
				WriteProcessMemory(hProcess, (LPVOID)(Address - 3), nop3, 3, NULL);
				Check += 1;
			}
			if (Check >= 2)
			{
				goto endfunc;
			}
		}
		MemoryBuff = MemoryBuff - 4096;
	}
endfunc:;
}




void Hook()
{
	//mainloop();
	bypass_online();
	while (true)
	{

	monitorButtons();
	optionCount = 0;

	

	if (submenu == 1)
	{
		Main();
	}
	else if (submenu == 2)
	{
		Selfmenu();
	}
	else if (submenu == 3)
	{
		PlayersList();
	}
	else if (submenu == 4)
	{
		OnlinePlayerMods();
	}
	else if (submenu == 5)
	{
		VehSpawner();
	}
	else if (submenu == 6)
	{
		PCCars();
	}
	else if (submenu == 7)
	{
		VehMenu();
	}
	else if (submenu == 8)
	{
		SelPVeh();
	}
	else if (submenu == 9)
	{
		Credits();
	}
	

	optionPress = false;
	WAIT(0);
	}
}



void ScriptMain()
{
	srand(GetTickCount());
	Hook();
}
